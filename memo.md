Project Status: Complete (Some functionality might be not accurately implemented)

Project Link: https://amiqdad@bitbucket.org/amiqdad/cs445-amiqdad.git

3320 lines of Code

2178 Lines of Test

69% approx method coverage
71% approx test coverage (classes)
62% approx line coverage

Cyclomatic complexity < 20

Time taken: 30 hours

Challenges:
---
Connecting database to sql
Learning and understanding rest principles
getting used to test driven development