package com.project.cs445.model;

import com.project.cs445.information.CustomerInfo;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */


public class Customer {

    private int id;
    private String fName;
    private String lName;
    private String phone;
    private String email;

    public Customer(String fName, String lName, String phone, String email) {

        this.fName = fName;
        this.lName = lName;
        this.phone = phone;
        this.email = email;

    }

    public Customer(CustomerInfo info) {
        String split[] = info.getName().split(" ");
        if(split.length < 2) {
            fName = "";
            lName = split[0];
        }
        else {
            fName = split[0];
            lName = split[1];
        }
        this.email = info.getEmail();
        this.phone = info.getPhone();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
