package com.project.cs445.model;

import java.util.Date;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class RevenueReport extends Report {

    private Date startDate;
    private Date endDate;
    private int orders_placed;
    private int orders_cancelled;
    private int orders_open;
    private double food_revenue;
    private double surcharge_revenue;


    public RevenueReport(int id, String s, Date startDate, Date endDate, int orders_placed, int orders_cancelled, int orders_open, double food_revenue, double surcharge_revenue) {
        super(id, s);
        this.startDate = startDate;
        this.endDate = endDate;
        this.orders_placed = orders_placed;
        this.orders_cancelled = orders_cancelled;
        this.orders_open = orders_open;
        this.food_revenue = food_revenue;
        this.surcharge_revenue = surcharge_revenue;
    }

}
