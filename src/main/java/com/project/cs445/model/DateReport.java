package com.project.cs445.model;

import com.project.cs445.information.OrderStruct;
import com.project.cs445.information.OrderStructDetailed;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class DateReport extends Report{

    private List<OrderStructDetailed> orders;

    public DateReport(int id, String s, List<OrderStructDetailed> orders) {
        super(id, s);
        this.orders = orders;
    }
}
