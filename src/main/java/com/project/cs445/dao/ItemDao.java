package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.OrderItem;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/15/16.
 */
public interface ItemDao {

    void add(OrderItem item) throws DaoException;

    List<OrderItem> findAll() throws DaoException;
    OrderItem findById(int id) throws DaoException;

}
