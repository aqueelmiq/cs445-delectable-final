package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.Order;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */

public interface OrderDao {

    void add(Order order) throws DaoException;

    public int setAmount(double amount, int orderNum) throws DaoException;

    public List<Order> findAll() throws DaoException;
    public List<Order> findByCustomerEmail(String id) throws DaoException;
    public List<Order> findByExactDate(Date Date) throws DaoException, ParseException;
    public List<Order> findByExactDeliveryDate(Date Date, String status) throws DaoException, ParseException;
    public List<Order> findByDate(Date startDate) throws DaoException, ParseException;
    public List<Order> findByDate(Date startDate, Date endDate) throws DaoException, ParseException;
    public List<Order> findByDeliveryDate(Date startDate) throws DaoException, ParseException;
    public List<Order> findByDeliveryDate(Date startDate, Date endDate) throws DaoException, ParseException;
    public List<Order> findOrder(int orderNum) throws DaoException;
    public  List<Order> findOrderByStatus(String status) throws DaoException;

    public Order findByOrderId(int id) throws DaoException;

    public int deliverOrder(int id) throws DaoException;
    public int cancelOrder(int id) throws DaoException;


}
