package com.project.cs445.dao;

import com.project.cs445.exc.DaoException;
import com.project.cs445.model.*;
import java.util.List;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */
public interface CustomerDao {

    void add(Customer customer) throws DaoException;

    List<Customer> findAll() throws DaoException;
    List<Customer> findByName(String lName) throws DaoException;
    Customer findByEmail(String email) throws DaoException;
    Customer findByPhone(String phone) throws DaoException;
    Customer findById(int id) throws DaoException;


}
