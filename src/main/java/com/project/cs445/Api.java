package com.project.cs445;


import com.google.gson.Gson;
import com.project.cs445.dao.*;
import com.project.cs445.exc.DaoException;
import com.project.cs445.information.*;
import com.project.cs445.model.*;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import spark.Response;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static spark.Spark.*;

/**
 * Created by aqueelmiqdad on 4/10/16.
 */

public class Api {

    public static final int ORDERNUMSTART = 100;


    public static void main(String args[]) throws DaoException, IOException {


        String dataSource = "jdbc:h2:~/appdb1.db";
        port(8080);
        if(args.length > 0) {
            if(args.length != 2){
                System.out.println("Invalid args count");
                System.exit(0);
            }
            port(Integer.parseInt(args[0]));
            dataSource = args[1];
        }

        String conString = dataSource + ";INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString, "", "");
        Writer store = new Writer("values.txt", "order.txt");
        FoodDao foodDao = new Sql2oFoodDao(sql2o);
        OrderDao orderDao = new Sql2oOrderDao(sql2o);
        ItemDao itemDao = new Sql2oItemDao(sql2o);
        CustomerDao custDao = new Sql2oCustomerDao(sql2o);

        //Keep connection open for testing
        Connection con = sql2o.open();
        Gson gson = new Gson();
        store.setSurcharge(0);

        /*
                For Customers
                1. GET Menu
                2. PUT Create Order
                3. GET Current Order
                5. POST Update (cancel/Deliver) current Order

         */

        //GET MENU (ALL) works
        get("/delectable/menu", "application/json", (req, res) -> {
            res.status(200);
            return foodDao.findAll();
        }, gson::toJson);

        //GET MENU (ID) works
        get("/delectable/menu/:id", "application/json", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            if(foodDao.findById(id) == null)
                res.status(404);
            else
                res.status(200);
            return foodDao.findById(id);
        }, gson::toJson);


        after((req, res) -> {
            res.type("application/json");
        });

         /*
                For Admins
                1. POST new fooditem
                2. POST update fooditem
                3. PUT fooditem
                4. POST Update surcharge

         */

        //Add food item works
        put("/delectable/admin/menu", "application/json", (req, res) -> {
            try {
                FoodStruct foodS = gson.fromJson(req.body(), FoodStruct.class);
                Food food = new Food(foodS);
                foodDao.addFood(food);
                Map<String, Integer> idMap = new HashMap();
                idMap.put("id", food.getId());
                res.status(200);
                return idMap;

            }catch (IllegalStateException ex) {
                res.status(400);
                return 0;
            } catch (java.lang.NullPointerException ec) {
                res.status(400);
                return 0;
            }


        }, gson::toJson);

        //Update food item works
        post("/delectable/admin/menu/:mid", "application/json", (req, res) -> {
            try {
                FoodPrice price = gson.fromJson(req.body(), FoodPrice.class);
                int id = Integer.parseInt(req.params("mid"));
                if(foodDao.updatePriceById(id, price.getPrice_per_person())==0)
                    res.status(400);
                else
                    res.status(200);
                return foodDao.findById(id);
            }
            catch (NullPointerException ex) {
                res.status(204);
                return "";
            }
        }, gson::toJson);

        //GET Surcharge
        get("/delectable/admin/surcharge", "application/json", (req, res) -> {
            res.status(200);
            return new surchargeStore(store.getSurcharge());
        }, gson::toJson);

        //Update Surcharge
        post("/delectable/admin/surcharge", "application/json", (req, res) -> {
            surchargeStore val = gson.fromJson(req.body(), surchargeStore.class);
            store.setSurcharge(val.getSurcharge());
            res.status(200);
            return val.getSurcharge();
        }, gson::toJson);


         /*
                For Report
                1. GET Order Count in last x months
                2. GET Order Revenue in last x months

         */

        get("/delectable/report", "application/json", (req, res) -> {
            List<Report> reports = new ArrayList<Report>();
            reports.add(new Report(801, "Orders to deliver today"));
            reports.add(new Report(802, "Orders to deliver tomorrow"));
            reports.add(new Report(803, "Revenue report"));
            reports.add(new Report(804, "Orders delivery report"));
            res.status(200);
            return reports;
        }, gson::toJson);

        //GET Order Report

        get("/delectable/report/:rid", "application/json", (req, res) -> {
            int rid = Integer.parseInt(req.params("rid"));
            List<Order> orders;
            List<OrderStructDetailed> orderInfos = new ArrayList<OrderStructDetailed>();
            int i = 0, oidTmp = 0, orderNum = 100;
            double total = 0, surchargeTotal = 0;
            String startDate = req.queryParams("start_date");
            String endDate = req.queryParams("end_date");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            Date start, end;
            switch (rid) {
                case 801:
                    return new DateReport(801, "Today's order report", (List<OrderStructDetailed>)dateBasedReport(new Date(), store, foodDao, orderDao, itemDao, custDao, res, orderInfos));
                case 802:
                    start = new Date();
                    end = new Date(start.getTime() + 1000*24*60*60);
                    return new DateReport(802, "Tomorrow's order report", (List<OrderStructDetailed>)dateBasedReport(end, store, foodDao, orderDao, itemDao, custDao, res, orderInfos));
                case 803:
                    if(startDate != null && endDate != null){
                        orders = orderDao.findByDate(formatter.parse(startDate), formatter.parse(endDate));
                        return revenueGenerate(orderDao, store, foodDao, itemDao, custDao, res, orders, startDate, endDate);
                    }
                    else if(startDate != null) {
                        orders = orderDao.findByDate(formatter.parse(startDate));
                        return revenueGenerate(orderDao, store, foodDao, itemDao, custDao, res, orders, startDate, startDate);
                    }
                    else {
                        orders = orderDao.findAll();

                    }
                    return revenueGenerate(orderDao, store, foodDao, itemDao, custDao, res, orders, startDate, endDate);

                case 804:

                    if(startDate != null && endDate != null) {
                        start = formatter.parse(startDate);
                        end = formatter.parse(endDate);
                        orders = orderDao.findByDate(start, end);
                    }
                    else if(startDate != null) {
                        start = formatter.parse(startDate);
                        orders = orderDao.findByDate(start);
                    }
                    else
                        orders = orderDao.findAll();
                    if(orders.size() > 0)
                        orderNum = orders.get(0).getOrderNum();
                    else {
                        res.status(401);
                        return null;
                    }
                    i = 1;
                    while(orderNum < store.getOrderNum() && i < orders.size()) {
                        List<Order> ordersTmp = orderDao.findOrder(orderNum);
                        OrderStructDetailed tmp = getDetailedOrder(ordersTmp, itemDao, custDao, foodDao);
                        oidTmp = orderNum;
                        while (i < orders.size() && oidTmp == orderNum){
                            orderNum = orders.get(i).getOrderNum();
                            i++;
                        }
                        if(tmp != null)
                            orderInfos.add(tmp);
                    }
                    DateReport dateReport = new DateReport(804, "Delivery Report", orderInfos);
                    res.status(200);
                    return dateReport;
                default:
                    return "Invalid rid";
            }
        }, gson::toJson);


        /*
            Orders::

                   1. PUT Order (Create new Order)
                   2. DELETE Order (Cancel Order)
                   3. GET Order (View Order)

         */

        //PUT order
        put("/delectable/order", "application/json", (req, res) -> {
            int orderNum = 0;
            double amount = 0;
            Order nOrder = new Order(0, "", 0, 1, "", 0.0, "open", new Date(), new Date(((new Date()).getTime()) + 1000*24*60*60), "");
            OrderStruct order = gson.fromJson(req.body(), OrderStruct.class);
            Customer customer;
            Food food;
            if((customer = custDao.findByEmail(order.getPersonal_info().getEmail())) == null) {
                customer = new Customer(order.getPersonal_info());
                custDao.add(customer);
            }
            for(itemStruct items: order.getOrder_detail()) {
                food = foodDao.findById(items.getId());
                if(food == null) {
                    return "Invalid Food id entered";
                }
                else if(food.getMinimum_order() > items.getCount()) {
                    return "invalid quantity - Quantity less than minimum";
                }
                OrderItem item = new OrderItem(food.getId(), items.getCount(), food.getPrice_per_person());
                itemDao.add(item);
                amount += item.getPrice()*item.getQty();
                orderNum = store.getOrderNum();
                nOrder = new Order(orderNum, customer.getEmail(), 0, item.getId(), order.getNote(), store.getSurcharge(), "open", new Date(), order.getDeliveryDate(), order.getDeliveryAddress());
                orderDao.add(nOrder);
            }
            orderDao.setAmount(amount, orderNum);
            orderNum++;
            store.setOrderNum(orderNum);
            res.status(200);
            Map orderMap = new HashMap();
            orderMap.put("id:", nOrder.getOrderNum());
            orderMap.put("cancel_url:", "/order/cancel/" + nOrder.getOrderNum());
            return orderMap;
        }, gson::toJson);

        //GET Order
        get("/delectable/order", "application/json", (req, res) -> {
            List<OrderStruct> listOrders = new ArrayList<OrderStruct>();
            for(int i = ORDERNUMSTART; i < store.getOrderNum(); i++) {
                OrderStruct order = getOrder(i, orderDao, itemDao, custDao);
                listOrders.add(order);
            }
            res.status(200);
            return listOrders;
        }, gson::toJson);

        //GET Order
        get("/delectable/order/:oid", "application/json", (req, res) -> {
            int id = Integer.parseInt(req.params("oid"));
            List<Order> list = orderDao.findOrder(id);
            if(list.size() == 0) {
                res.status(404);
                return "404 not found";
            }
            OrderStructDetailed listOrders = getDetailedOrder(list, itemDao, custDao, foodDao);
            res.status(200);
            return listOrders;
        }, gson::toJson);

        //Cancel Order
        post("/delectable/order/cancel/:oid", "application/json", (req, res) -> {
            if(req.body() == null) {
                res.status(204);
                return "";
            }
            int id = Integer.parseInt(req.params("oid"));
            Order order = orderDao.findByOrderId(id);
            Date orderDate = order.getDeliveryDate();
            Date today = new Date();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            if(fmt.format(today).equals(fmt.format(orderDate))) {
                res.status(400);
                return "Cannot cancel today's order - too late";
            }
            else {
                orderDao.cancelOrder(id);
                res.status(200);
                return "{ " +
                        " id = " + id + "cancelled" +
                        "}";
            }
        }, gson::toJson);

        //Deliver Order
        post("/delectable/order/delivery/:oid", "application/json", (req, res) -> {
            int id = Integer.parseInt(req.params("oid"));
            Order order = orderDao.findByOrderId(id);
            if(order == null) {
                res.status(200);
                return "Order Not Found";
            }
            orderDao.deliverOrder(id);
            return null;

        }, gson::toJson);


        /*
                For Customer Service
                1. GET Order by Customer
                2. GET Orders for Today
                3. GET Orders for Tomorrow

         */

        //GET ALL CUSTOMERS
        get("/delectable/customer", "application/json", (req, res) -> {
            String last = req.queryParams("key");
            List<Customer> cust;
            Customer customer;
            res.status(200);
            if(last != null) {
                if((cust = custDao.findByName(last)).size() != 0)
                    return cust;
                else if((customer = custDao.findByEmail(last)) != null)
                    return customer;
                else if ((customer = custDao.findByPhone(last)) != null)
                    return customer;
                else {
                    res.status(404);
                    return null;
                }
            }
            else
                return custDao.findAll();
        }, gson::toJson);


        //GET DETAILED SPECIFIC CUSTOMER
        get("/delectable/customer/:cid", "application/json", (req, res) -> {
            int id = Integer.parseInt(req.params("cid"));
            Customer customer = custDao.findById(id);
            List<Order> orders = orderDao.findByCustomerEmail(customer.getEmail());
            int i = 0, oidTmp = orders.get(0).getOrderNum();
            List<OrderInfo> orderInfos = new ArrayList();
            while (i < orders.size()) {
                orderInfos.add(new OrderInfo(orders.get(i)));
                while(i < orders.size() && oidTmp == orders.get(i).getOrderNum()) {
                    i++;
                }
                if(i < orders.size())
                    oidTmp = orders.get(i).getOrderNum();
            }
            return new CustomerOrders(customer, orderInfos);
        }, gson::toJson);

        /*
                OTHER
         */

    con.close();

    }

    public static Object revenueGenerate(OrderDao orderDao, Writer store, FoodDao foodDao, ItemDao itemDao, CustomerDao custDao, Response res, List<Order> orders, String start, String end) throws FileNotFoundException, DaoException, ParseException {
        RevenueReport rev;
        int orderNum, orders_placed = 0, orders_cancelled = 0, orders_open = 0, oidTmp = 0;
        double total = 0, surchargeTotal = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int i = 1;
        if(orders.size() > 0)
            orderNum = orders.get(0).getOrderNum();
        else {
            res.status(404);
            return null;
        }
        while (orderNum < store.getOrderNum() && i < orders.size()) {
            List<Order> ordersTmp = orderDao.findOrder(orderNum);
            OrderStructDetailed tmp = getDetailedOrder(orders, itemDao, custDao, foodDao);
            oidTmp = orderNum;
            if (tmp != null) {
                if (tmp.getStatus() == null)
                    tmp.setStatus("open");
                if (tmp.getStatus().toLowerCase().equals("open"))
                    orders_open++;
                else if (tmp.getStatus().toLowerCase().equals("cancelled"))
                    orders_cancelled++;
                orders_placed++;
                total += tmp.getAmount();
                surchargeTotal += tmp.getSurcharge();
                while (i < orders.size() && oidTmp == orderNum){
                    orderNum = orders.get(i).getOrderNum();
                    i++;
                }
            }
        }
        res.status(200);
        if(start == null && end == null)
            rev = new RevenueReport(803, "Revenue Report", null, null, orders_placed, orders_cancelled, orders_open, total, surchargeTotal);
        else if(start != null && end != null)
            rev = new RevenueReport(803, "Revenue Report", formatter.parse(start), formatter.parse(end), orders_placed, orders_cancelled, orders_open, total, surchargeTotal);
        else if(start != null)
            rev = new RevenueReport(803, "Revenue Report", formatter.parse(start), null, orders_placed, orders_cancelled, orders_open, total, surchargeTotal);
        else {
            res.status(401);
            rev = null;
        }
        return rev;
    }

    public static Object dateBasedReport(Date date, Writer store, FoodDao foodDao, OrderDao orderDao, ItemDao itemDao, CustomerDao custDao, Response res, List<OrderStructDetailed> orderInfos) throws DaoException, ParseException, FileNotFoundException {

        List<Order> orders;
        int orderNum, oidTmp;
        orders = orderDao.findByExactDeliveryDate(date, "open");
        if(orders.size() < 0) {
            res.status(404);
            return null;
        }
        if(orders.size() > 0)
            orderNum = orders.get(0).getOrderNum();
        else {
            res.status(404);
            return null;
        }
        int i = 1;
        while(orderNum < store.getOrderNum() && i < orders.size()) {
            List<Order> ordersTmp = orderDao.findOrder(orderNum);
            OrderStructDetailed tmp = getDetailedOrder(ordersTmp, itemDao, custDao, foodDao);
            oidTmp = orderNum;
            while (i < orders.size() && oidTmp == orderNum){
                orderNum = orders.get(i).getOrderNum();
                i++;
            }
            if(tmp != null)
                orderInfos.add(tmp);
        }
        return orderInfos;
    }

    public static OrderStruct getOrder(int orderNum, OrderDao orderDao, ItemDao itemDao, CustomerDao customerDao) throws DaoException {
        List<Order> data = orderDao.findOrder(orderNum);
        if(data.size() == 0)
            return null;
        Customer customer = customerDao.findByEmail(data.get(0).getOrdered_by());
        List<itemStruct> items = new ArrayList<>();
        for(Order item: data) {
            itemStruct tmp = new itemStruct(itemDao.findById(item.getId()));
            items.add(tmp);
        }
        OrderStruct order = new OrderStruct(data.get(0), customer, items);
        return order;
    }

    public static OrderStructDetailed getDetailedOrder(List<Order> dt, ItemDao itemDao, CustomerDao customerDao, FoodDao foodDao) throws DaoException {
        List<Order> data = dt;
        OrderStructDetailed order;
        if(data.size() == 0)
            return new OrderStructDetailed();
        Customer customer = customerDao.findByEmail(data.get(0).getOrdered_by());
        List<itemStructDetail> items = new ArrayList<>();
        for(Order item: data) {
            OrderItem orderItem = itemDao.findById(item.getId());
            itemStructDetail tmp = new itemStructDetail(orderItem, foodDao.findById(orderItem.getFoodid()).getName());
            items.add(tmp);
        }
        order = new OrderStructDetailed(data.get(0), customer, items);
        return order;
    }




}
