package com.project.cs445.information;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/25/16.
 */
public class FoodStruct extends FoodPrice {

    private String name;
    private int minimum_order;
    private List<Category> categories;

    public FoodStruct(int id, double price_per_person, String name, int minimum_order, List<Category> categories ) {
        super(id, price_per_person);
        this.categories = categories;
        this.name = name;
        this.minimum_order = minimum_order;
    }

    public String getName() {
        return name;
    }

    public int getMinimum_Order() {
        return minimum_order;
    }

    public String getCategories() {
        return categories.toString();
    }
}
