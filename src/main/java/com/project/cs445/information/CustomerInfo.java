package com.project.cs445.information;

import com.project.cs445.model.Customer;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/23/16.
 */
public class CustomerInfo {
    private String name;
    private String email;
    private String phone;

    public CustomerInfo(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    public CustomerInfo(Customer customer) {
        this.name = customer.getfName() + " " + customer.getlName();
        this.email = customer.getEmail();
        this.phone = customer.getPhone();
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

}
