package com.project.cs445.information;

import com.project.cs445.model.Customer;

import java.util.List;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class CustomerOrders {

    private String name;
    private String email;
    private String phone;
    private List<OrderInfo> orders;


    public CustomerOrders(Customer c, List<OrderInfo> orders) {
        this.name = c.getfName() + " " + c.getlName();
        this.email = c.getEmail();
        this.phone = c.getPhone();
        this.orders = orders;
    }

}
