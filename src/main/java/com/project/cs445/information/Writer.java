package com.project.cs445.information;

import java.io.*;
import java.util.Scanner;

/**
 * Created by aqueelmiqdad on 4/12/16.
 */
public class Writer {

    private String filename;
    private String filename2;
    private File file;
    private File file2;

    public Writer(String filename, String filename2) {
        this.filename = filename;
        this.filename2 = filename2;
    }

    public Writer(String filename) {
        this.filename = filename;
    }

    public int getOrderNum() throws FileNotFoundException {
        file2 = new File(filename2);
        Scanner in = new Scanner(file2);
        int orderNum = in.nextInt();
        in.close();
        return orderNum;
    }

    public void setOrderNum(int value) throws IOException {

        File file2 = new  File(filename2);

        // if file doesnt exists, then create it

        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileWriter fw = null;
        try {
            fw = new FileWriter(file2.getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bw = new BufferedWriter(fw);
        try {
            bw.write((value+""));
        } catch (IOException e) {
            e.printStackTrace();
        }
        bw.close();

    }

    public double getSurcharge() throws FileNotFoundException {
        file = new File(filename);
        Scanner in = new Scanner(file);
        double surcharge = in.nextDouble();
        in.close();
        return surcharge;
    }

    public void setSurcharge(double value) throws IOException {

        File file = new  File(filename);

        // if file doesnt exists, then create it

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileWriter fw = null;
        try {
            fw = new FileWriter(file.getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bw = new BufferedWriter(fw);
        try {
            bw.write((value+""));
        } catch (IOException e) {
            e.printStackTrace();
        }
        bw.close();

    }


}
