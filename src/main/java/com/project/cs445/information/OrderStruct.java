package com.project.cs445.information;

import com.project.cs445.model.Customer;
import com.project.cs445.model.Order;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by aqueelmiqdad on 4/19/16.
 */

public class OrderStruct {

    private Date delivery_date;
    private String delivery_address;
    private double amount;
    private double surcharge;
    private CustomerInfo personal_info;
    private String note;
    private List<itemStruct> order_detail;
    private String status;
    

    public OrderStruct(Order order, Customer customer, List<itemStruct> order_detail) {
        this.delivery_date = order.getDeliveryDate();
        this.delivery_address = order.getDeliveryAddress();
        this.note = order.getSpecial();
        this.order_detail = order_detail;
        this.personal_info = new CustomerInfo(customer);
        this.amount = order.getAmount();
        this.surcharge = order.getSurcharge();
        this.status = order.getStatus();

    }

    public String getNote() {
        return note;
    }

    public List<itemStruct> getOrder_detail() {
        return order_detail;
    }

    public Date getDeliveryDate() {
        return delivery_date;
    }

    public String getDeliveryAddress() {
        return delivery_address;
    }

    public CustomerInfo getPersonal_info() {
        return personal_info;
    }
}
