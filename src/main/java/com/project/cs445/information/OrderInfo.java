package com.project.cs445.information;

import com.project.cs445.model.Order;

import java.util.Date;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */

public class OrderInfo {

    private int orderNum;
    private String ordered_by;
    private String note;
    private double surcharge;
    private String status;
    private Date orderDate;
    private Date deliveryDate;
    private String deliveryAddress;

    public OrderInfo(Order order) {
        this.orderNum = order.getOrderNum();
        this.ordered_by = order.getOrdered_by();
        this.note = order.getSpecial();
        this.surcharge = order.getSurcharge();
        this.status = order.getStatus();
        this.orderDate = order.getOrderDate();
        this.deliveryDate = order.getDeliveryDate();
        this.deliveryAddress = order.getDeliveryAddress();
    }



}
