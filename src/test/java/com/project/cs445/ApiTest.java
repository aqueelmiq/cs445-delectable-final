package com.project.cs445;

import com.google.gson.Gson;
import com.project.cs445.dao.Sql2oCustomerDao;
import com.project.cs445.dao.Sql2oFoodDao;
import com.project.cs445.dao.Sql2oItemDao;
import com.project.cs445.dao.Sql2oOrderDao;
import com.project.cs445.information.*;
import com.project.cs445.model.Customer;
import com.project.cs445.model.Food;
import com.project.cs445.model.Order;
import com.project.cs445.model.OrderItem;
import com.project.cs445.testing.ApiClient;
import com.project.cs445.testing.ApiResponse;
import org.junit.*;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import spark.Spark;

import java.util.*;

import static java.lang.Thread.sleep;
import static org.junit.Assert.*;

/**
 * Created by aqueelmiqdad on 4/24/16.
 */
public class ApiTest {

    public static final String PORT = "4560";
    public static final String TESTING_SOURCE = "jdbc:h2:mem:testing";
    private Sql2o sql2o;
    private Connection conn;
    private Gson gson;
    private ApiClient client;
    private Sql2oOrderDao dao;
    private Sql2oFoodDao daoF;
    private Sql2oItemDao daoI;
    private Sql2oCustomerDao daoC;

    @BeforeClass
    public static void startServer() throws Exception {
        String[] args = {PORT, TESTING_SOURCE};
        Api.main(args);
    }

    @Before
    public void setUp() throws Exception {
        sleep(50);
        String conString = TESTING_SOURCE + ";INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        sql2o = new Sql2o(conString , "", "");
        client = new ApiClient("http://localhost:4560");
        conn = sql2o.open();
        gson = new Gson();
        dao = new Sql2oOrderDao(sql2o);
        daoF = new Sql2oFoodDao(sql2o);
        daoI = new Sql2oItemDao(sql2o);
        daoC = new Sql2oCustomerDao(sql2o);
        Food food = sampleFood();
        daoF.addFood(food);

        OrderItem item = sampleItem(food);
        daoI.add(item);

        Customer cust = sampleCustomer();
        daoC.add(cust);

        Order order = sampleOrder(item, cust);
        int originalId = order.getId();
        dao.add(order);
    }

    @After
    public void tearDown() throws Exception {
        conn.close();
    }

    @AfterClass
    public static void stopServer() throws Exception {
        Spark.stop();
    }

    //SURCHARGE

    @Test
    public void putSurchargeWorks() throws Exception {

        surchargeStore store = new surchargeStore(3.5);
        ApiResponse response = client.request("POST", "/delectable/admin/surcharge", gson.toJson(store));

        assertEquals(200, response.getStatus());

    }


    @Test
    public void getSurchargeWorks() throws Exception {

        ApiResponse response = client.request("GET", "/delectable/admin/surcharge");

        assertEquals(200, response.getStatus());

    }

    //MENU

    //GET MENU
    @Test
    public void getMenuWorks() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);

        ApiResponse response = client.request("GET", "/delectable/menu");

        assertEquals(200, response.getStatus());

    }

    //GET MENU ID
    @Test
    public void getMenuIDWorks() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);
        ApiResponse response = client.request("GET", "/delectable/menu/1");

        assertEquals(200, response.getStatus());

    }

    //POST MENU

    @Test
    public void postMenuPriceWorks() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);
        FoodPrice price = new FoodPrice(1, 10);
        ApiResponse response = client.request("POST", "/delectable/admin/menu/1", gson.toJson(price));

        assertEquals(200, response.getStatus());

    }

    @Test
    public void postMenuPriceWorksWell() throws Exception {

        Food food = sampleFood();
        daoF.addFood(food);
        FoodPrice price = new FoodPrice(1, 10);
        ApiResponse response = client.request("POST", "/delectable/admin/menu/1", gson.toJson(price));

        assertEquals((int)price.getPrice_per_person(), (int)daoF.findById(1).getPrice_per_person());

    }

    @Test
    public void putMenuWorks() throws Exception {


        List<Category> categories = new ArrayList<Category>();
        categories.add(new Category("hi"));
        Map food = new HashMap<>();
        food.put("name", "blah");
        food.put("categories", categories);
        food.put("price_per_person", 2);
        food.put("minimum_order", 100);

        ApiResponse response = client.request("PUT", "/delectable/admin/menu", gson.toJson(food) );

        assertEquals(200, response.getStatus());

    }

    //Invalid Menu
    @Test
    public void putIvalidMenuNotWorks() throws Exception {

        Food food = sampleFood();

        ApiResponse response = client.request("PUT", "/delectable/admin/menu", "");

        assertEquals(400, response.getStatus());

    }

    //ORDER

    //GET ORDER
    @Test
    public void getOrderWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/order");

        assertEquals(200, response.getStatus());

    }

    //PUT ORDER
    @Test
    public void putOrderWorks() throws Exception {

        CustomerInfo info = new CustomerInfo("Aqueel", "a@a.com", "3125369986");
        List<itemStruct> items = new ArrayList();
        items.add(new itemStruct(new OrderItem(1, 8, 12)));

        Map order = new HashMap();
        order.put("delivery_date", "20160301");
        order.put("delivery_address", "10 West 31st ST, Chicago IL 60616");
        order.put("personal_info", info);
        order.put("note", "Room SB-214");
        order.put("order_detail", items);

        ApiResponse response = client.request("PUT", "/delectable/order", gson.toJson(order));

        assertEquals(200, response.getStatus());

    }

    //Customer

    @Test
    public void getCustomerWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/customer");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getReoprtWorks() throws Exception {



        ApiResponse response = client.request("GET", "/delectable/report");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getRevenueReoprtWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/report/803");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getTodayReportNotFoundWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/report/801");

        assertEquals(404, response.getStatus());

    }

    @Test
    public void getTomorrowReportFoundWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/report/802");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getRevReportFoundWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/report/803");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getDateReportFoundWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/report/804");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getTomorrowReportWorks() throws Exception {



        ApiResponse response = client.request("GET", "/delectable/report/802");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getDeliveryReportWorks() throws Exception {



        ApiResponse response = client.request("GET", "/delectable/report/804");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void getCustomersWorks() throws Exception {

        ApiResponse response = client.request("GET", "/delectable/customer");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void CustomerOrderWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/customer/1");

        assertEquals(200, response.getStatus());

    }

    @Test
    public void CustomernameWorks() throws Exception {


        ApiResponse response = client.request("GET", "/delectable/customer?key=Jordan");

        assertEquals(200, response.getStatus());

    }

    private Order sampleOrder(OrderItem item, Customer cust) {
        return new Order(100, cust.getEmail(), 0, item.getId(), "baby", 0.75, "open", new Date(), new Date(((new Date()).getTime()) + 1000*24*60*60), "blah blah blah");
    }

    private OrderItem sampleItem(Food food) {
        return new OrderItem(food.getId(), food.getMinimum_order()*10, food.getPrice_per_person());
    }

    private Customer sampleCustomer() {
        return new Customer("Mike", "Jordan", "3215676780", "mike.jordan@gmail.com");
    }

    private Food sampleFood() {
        return new Food("Chicken Soup", "Non-Veg", 1.5, 35);
    }
}